#include <iostream>

// -- interfaces / prototypes --
bool is_leap_year (const size_t year);
size_t zeller_algo (const size_t year);
size_t num_of_days (const size_t month, const size_t year);
double count_wedns (const size_t start_year, const size_t end_year);

int main (const int argc, const char** argv) {
  if (argc != 3) {
    std::cerr << "USAGE: " << argv[0] << " start_year end_year" << std::endl;
    return EXIT_FAILURE;
  }
  size_t start_year = (size_t) std::atoi(argv[1]), end_year = (size_t) std::atoi(argv[2]);
  std::cout << count_wedns(start_year, end_year) << std::endl;
  return EXIT_SUCCESS;
}

// returns whether a given year is a leap year or not
bool is_leap_year (const size_t year) {
  return (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0);
}

// returns the total number of days within a given month
size_t num_of_days (const size_t month, const size_t year) {
  if (month == 2) return is_leap_year(year) ? 29 : 28;
  else if (month == 4 || month == 6 || month == 9 || month == 11) return 30;
  else return 31;
}

// returns the 1st occurrence of a wednesday in a given year
// more on that: 'https://en.wikipedia.org/wiki/Zeller%27s_congruence'
// 0:Saturday, 1:Sunday, 2:Monday, 3:Tuesday, 4:Wednesday, 5:Thursday, 6:Friday
size_t zeller_algo (const size_t year) {
  size_t weekday = (1 + (13 * (13 + 1) / 5) + (year-1) + ((year-1) / 4) - ((year-1) / 100) + ((year-1) / 400)) % 7;
  return weekday <= 4 ? 5-weekday : 12-weekday;
}

// returns the percentage of wednesdays that fall on the last day of a month
double count_wedns (const size_t start_year, const size_t end_year) {
  size_t total_wedns = 0, wedns_on_end_of_month = 0;
  for (size_t curr_year = start_year; curr_year <= end_year; ++curr_year) {
    for (size_t curr_month = 1; curr_month <= 12; ++curr_month) {
      size_t curr_day = curr_month == 1 && curr_year == start_year ? zeller_algo(curr_year) : curr_day - num_of_days(curr_month - 1, curr_year);
      size_t num_days = num_of_days(curr_month, curr_year);
      for (; curr_day <= num_days; curr_day += 7) {
        if (curr_day == num_days)
          ++wedns_on_end_of_month;
        ++total_wedns;
      }
    }
  }
  return (double) 100 * wedns_on_end_of_month / total_wedns;
}
