#include <iostream>
#include <random>

double simulate (const size_t trials);

int main (const int argc, const char** argv) {
  if (argc != 2) {
    std::cerr << "USAGE: " << argv[0] << " trials" << std::endl; 
    return EXIT_FAILURE;
  }
  size_t trials = (size_t) std::atoi(argv[1]);
  std::cout << simulate(trials) << std::endl;
  return EXIT_SUCCESS;
}

// runs a monte-carlo simulation to obtain a 
// numerical approximation of the expected value
// I use default_random_engine instead of the
// canonical random_device to ensure numerical
// results are machine-independent and reproducible 
// in this subroutine.
double simulate (const size_t trials) {
  std::default_random_engine seeder;
  std::mt19937_64 generator(seeder());
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  double c1, c2, r1, r2, s1, s2, s3, sigma = 0.0;
  for (size_t trial = 0; trial < trials; ++trial) {
    c1 = distribution(generator), c2 = distribution(generator);
    r1 = std::min(c1, c2), r2 = std::max(c1, c2);
    s1 = r1, s2 = r2 - r1, s3 = 1 - r2;
    sigma += std::max(std::max(s1, s2), s3);
  }
  return sigma / trials;
}
